# **Story 9 PPW: AJAX Dynamic Data Access**

## Status pipelines
[![pipeline status](https://gitlab.com/salsabilamaurizka/labppw-6/badges/master/pipeline.svg)](https://gitlab.com/salsabilamaurizka/labppw-6/commits/master)


## Coverage Report
[![coverage report](https://gitlab.com/salsabilamaurizka/labppw-6/badges/master/coverage.svg)](https://gitlab.com/salsabilamaurizka/labppw-6/commits/master)

## Link Herokuapp
http://ppw6.herokuapp.com/
