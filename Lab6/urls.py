from django.urls import path
from . import views
from django.conf.urls import url
app_name = 'Lab6'



#path for app
urlpatterns = [
    path('status/', views.index, name='index'),
    path('delete/', views.delete_obj, name='delete'),
    path('', views.profil, name='profil'),
    path('daftarbuku/api/library/<str:teks>/', views.books, name='books'),
    path('daftarbuku/',views.daftarbuku,name='daftarbuku'),
    path('regis/', views.regis, name = 'regis'),
    path('add_subs/', views.add_subs, name = 'add_subs'),
    path('show_subs/',views.show_subs,name='show_subs'),
    path('delete_subs/',views.delete_subs,name='delete_subs'),
    path('login/',views.login,name='login'),
    path('logoutt/',views.logout_views,name='logout_views'),



]
