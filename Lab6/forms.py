from django import forms
from .models import Status
from datetime import datetime


class Status_Form(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        widgets = {
            'status': forms.Textarea(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(Status_Form, self).__init__(*args, **kwargs)
        self.fields['status'].label = "Masukkan status disini"

class Subscribe_Form(forms.Form):
    error_messages={
        'required':'Required Field'
    }
    name_attrs={
        'type':'text',
        'class':'form-control',
        'placeholder': 'Required'
    }
    email_attrs={
        'type':'email',
        'class': 'form-control',
        'placeholder':'Required'
    }
    password_attrs={
        'type':'password',
        'class':'form-control',
        'placeholder':'Required'
    }

    name = forms.CharField(
        label='Name',
        required = True,
        widget=forms.TextInput(attrs=name_attrs)
    )

    email = forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs)
    )

    password = forms.CharField(
        label='Password',
        required=True,
        widget=forms.PasswordInput(attrs=password_attrs)
    )
