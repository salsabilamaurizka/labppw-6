# Generated by Django 2.1.1 on 2018-11-21 18:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Lab6', '0002_subscriber'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='email',
            field=models.CharField(max_length=30, unique=True),
        ),
        migrations.AlterField(
            model_name='subscriber',
            name='name',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='subscriber',
            name='password',
            field=models.CharField(max_length=15),
        ),
    ]
