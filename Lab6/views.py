from django.shortcuts import render,redirect
from .forms import Status_Form
from .models import Status
from .forms import Subscribe_Form
from .models import Subscriber
from django.http import JsonResponse
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from datetime import datetime,date

import urllib.request, json
import requests


# Create your views here.

response = {}

def index(request):
    if request.method == 'POST':
        form = Status_Form(request.POST)
        statuses = Status.objects.all().order_by('date')
        if form.is_valid():
            status = Status(status=form.cleaned_data['status'])
            status.save()
            return redirect('Lab6:index')

    else:
        form = Status_Form()
        statuses = Status.objects.all().order_by('date')
    return render(request, 'index.html', {'form': form, 'statuses': statuses})

def delete_obj(request):
    Status.objects.all().delete()
    return redirect('Lab6:index')


def profil(request):
    return render(request, 'profil.html')

def books(request, teks):
    raw = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + teks).json()
    itemss = []
    for i in raw['items']:
        data = {}
        data['title'] = i['volumeInfo']['title']
        data['thumbnail'] = i['volumeInfo']['imageLinks']['thumbnail'] if 'imageLinks' in i['volumeInfo'].keys() else 'Tidak ada'
        data['authors']= ", ".join(i['volumeInfo']['authors']) if 'authors' in i['volumeInfo'].keys() else 'Tidak ada'
        data['publishedDate'] = i['volumeInfo']['publishedDate'] if 'publishedDate' in i['volumeInfo'].keys() else 'Tidak ada'
        itemss.append(data)
    return JsonResponse({"data" : itemss})

def daftarbuku(request):
    return render(request,'daftarbuku.html')



def regis(request):
    form = Subscribe_Form(request.POST)
    html = 'subscribe.html'
    return render(request,html,{'form':form})

def add_subs(request):
    response['form'] = Subscribe_Form()
    form = Subscribe_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        status = 'True'
        #Store account into subscribers database
        try:
            storedata = Subscriber(
            name=response['name'],
            email=response['email'],
            password=response['password']
            )
            storedata.save()
            return JsonResponse({'status':status})
        except IntegrityError as e:
            status = 'False'
            return JsonResponse({'status':status})
    status = 'False'
    return JsonResponse({'status':status})

def show_subs(request):
    data = list( Subscriber.objects.all().values('pk','name','email','password'))
    return JsonResponse({'subsdata':data})

@csrf_exempt
def delete_subs(request):
    if(request.method == 'POST'):
        subsdata = request.POST['pk']
        Subscriber.objects.filter(pk=subsdata).delete()
        return HttpResponse({'status':'Unsubscribe success!'})
    else:
        return HttpResponse({'status':'Failed to load page'})



def login(request):
    form = Subscribe_Form()
    return render(request,'login.html',{"form":form})

def logout_views(request):
    logout(request)
    return redirect('Lab6:daftarbuku')
