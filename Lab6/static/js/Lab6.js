//accordion
$(document).ready(function(){
    $('.accordion').find('h3').click(function(){

        //Expand or collapse this panel
        $(this).next().slideToggle('fast');

        //Hide the other panels
        $("#acc-panel.bg-white").not($(this).next()).slideUp('fast');

      });
//changetheme
    var toggle = true
    $("#id_change").on('click', function(){
        if(toggle){
            $("body").css({
                'background' : "url('https://scontent-sin2-2.xx.fbcdn.net/v/t1.0-9/45641485_1430920867040458_1992353578206887936_n.jpg?_nc_cat=103&_nc_ht=scontent-sin2-2.xx&oh=8cd5000ec99326780eb179964ede2a29&oe=5C77568F')",
                'color' : 'black',
            })
            $("#id_change").removeClass("btn-dark")
            $("#id_change").addClass("btn-light")
            $(".btn-warning,rounded").addClass("btn-dark")
            $(".btn-warning,rounded,btn-dark").removeClass("btn-warning")
            $("p").css('color', 'black')
            toggle = false
        }
        else{
            $("body").css({
                'background' : "url('https://scontent-sin2-2.xx.fbcdn.net/v/t1.0-9/45514922_1430920710373807_4703793202702843904_n.jpg?_nc_cat=102&_nc_ht=scontent-sin2-2.xx&oh=30ff0337d17a2b3990842e03032b16c3&oe=5C452604')",
                'color' : 'white',
            })
            $("#id_change").removeClass("btn-light")
            $("#id_change").addClass("btn-dark")
            $(".btn-dark,rounded").addClass("btn-warning")
            $(".btn-dark,rounded,btn-warning").removeClass("btn-dark")
            toggle = true
        }
    });
    //button architecture, comic, quilting
    $("#quilting").click(function() {
        process('quilting');
    });

    $("#architecture").click(function() {
        process('architecture');
    });

    $("#comic").click(function() {
        process('comic');
    });

    //Favorites
    $(document).on('click', '#buttonStar', function() {
            if( $(this).hasClass('clicked') ) {
                count -=1;
                $(this).removeClass('clicked');
            }
            else {
                $(this).addClass('clicked');
                count = count + 1;
            }
            $('.counters').html(count);

        });

});
//loading page
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

var count = 0;
//library
function process(teks) {
  $.ajax({
    url : 'api/library/' + teks,
    type : 'GET',
    dataType : 'json',
    success : function(data) {
      count = 0;
      $('.counters').html(count);
      $('tbody').empty();
      var print ='<tr>';
      console.log(data.data[0].title);
        for (var i = 1; i < data.data.length; i++) {
          print += '<td scope = "row">' + i +  '</td> <td>' + data.data[i-1].title + '</td> <td>';
          print += '<img src=\"' + data.data[i-1].thumbnail+ '\">' + '</td> <td>';
          print += data.data[i-1].authors+'</td> <td>';
          print += data.data[i-1].publishedDate+'</td> <td>';
          print +=' <button id="buttonStar" type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="klik tepat pada icon star"><i id="star"class ="fa fa-star" style="font-size:24px"></i></button> </td></tr>';
        }
        $('tbody').append(print);
    },
    error: function() {
      alert('error');
    }
  });
}

//story11
function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  document.getElementById('profileinfo').innerHTML = "Hey welcome " + profile.getName()+"<br>"
      + "<img src='"+ profile.getImageUrl()+"'/>";

}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}
