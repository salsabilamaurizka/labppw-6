$(document).ready(function(){
    $('#subs-button').closest('form')
    .submit(function() {
        return checkForm.apply($(this).find(':input')[0]);
    })
    .find(inputSelector).keyup(checkForm).keyup();

    $('#subsform').submit(function(e){
        e.preventDefault();
        $.ajax({
            method:'POST',
            url:'/add_subs/',
            data:$('form').serialize(),
            success:function(status){
                if (status.status == 'True') {
                    $('#alertt').html("<div class='alert alert-success' role='alert'>Thank you for subscribing!</div>");
                }else{
                    $('#alertt').html("<div class='alert alert-danger' role='alert'>Email is already used</div>");
                }
            }

        });
        return false;
    });

    var toggle = true
    $("#id_change").on('click', function(){
        if(toggle){
            $("body").css({
                'background' : "url('https://scontent-sin2-2.xx.fbcdn.net/v/t1.0-9/45641485_1430920867040458_1992353578206887936_n.jpg?_nc_cat=103&_nc_ht=scontent-sin2-2.xx&oh=8cd5000ec99326780eb179964ede2a29&oe=5C77568F')",
                'color' : 'black',
            })
            $("#id_change").removeClass("btn-dark")
            $("#id_change").addClass("btn-light")
            $(".btn-warning,rounded").addClass("btn-dark")
            $(".btn-warning,rounded,btn-dark").removeClass("btn-warning")
            $("#hello").css('color', 'black')
            $("h1,#hello1").css('color', 'black')
            $("p").css('color', 'black')
            toggle = false
        }
        else{
            $("body").css({
                'background' : "url('https://scontent-sin2-2.xx.fbcdn.net/v/t1.0-9/45514922_1430920710373807_4703793202702843904_n.jpg?_nc_cat=102&_nc_ht=scontent-sin2-2.xx&oh=30ff0337d17a2b3990842e03032b16c3&oe=5C452604')",
                'color' : 'white',
            })
            $("#id_change").removeClass("btn-light")
            $("#id_change").addClass("btn-dark")
            $(".btn-dark,rounded").addClass("btn-warning")
            $(".btn-dark,rounded,btn-warning").removeClass("btn-dark")
            $("#hello").css('color', 'white')
            $("h1,#hello1").css('color', 'white')
            $("p").css('color', 'white')
            toggle = true
        }
    });

    $.ajax({
        url:'/show_subs/',
        type:'GET',
        dataType:'json',
        success: function(data){
            var print = ('<tr>');
            for(var i = 1;i<data.subsdata.length;i++){
                print+= '<td scope ="row">' + i +'</td>';
                print+= '<td scope ="row">' + data.subsdata[i].name +'</td>';
                print+= '<td>'+data.subsdata[i].email+'</td>';
                print+= '<td>' +'<button class="btn btn-default" id="unsubs-button" onClick="deleteSubs('+data.subsdata[i].pk+')"type="submit">Unsubscribe</button></td></tr>';
            }
            console.log(print);
            $('#subs-table').append(print);
        }
    });
    $('#unsubs-button').click(function(){

    });

});

const inputSelector = ':input[Required]:visible';
function checkForm() {
  var isValidForm = true;
  $(this.form).find(inputSelector).each(function() {
    if (!this.value.trim()) {
      isValidForm = false;
    }
  });
  $(this.form).find('#subs-button').prop('disabled', !isValidForm);
  return isValidForm;
}

function deleteSubs(pk){
    console.log(pk)
    $.ajax({
        method:'POST',
        url:'/delete_subs/',
        data:{'pk':pk},
        success:function(){
            alert("Bye! :)))")
            window.location.reload();
        }
    });
}
