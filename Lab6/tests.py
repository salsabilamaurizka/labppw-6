from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time

from . import views
from .models import Status
from .models import Subscriber
from .forms import Status_Form
from .forms import Subscribe_Form

# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_Lab6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    # def test_story6_using_template(self):
    #     response = Client.get('/')
    #     self.assertTemplateUsed(response,'index.html')

    def test_Lab6_has_hello_apa_kabar(self):
        request = HttpRequest()
        response = views.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Status.objects.create(
            status='mengerjakan story ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_Lab6_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('/status', {'status': test})
        self.assertEqual(response_post.status_code, 301)

        response = Client().get('/status')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)


    def test_Lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status', {'status': test})
        self.assertEqual(response_post.status_code, 301)

        response = Client().get('/status')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_Lab6_delete_all_has_0_count(self):
        request = HttpRequest()
        response = views.delete_obj(request)
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 0)


#profil.html
    def test_Lab6_profil_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 404)

    def test_Lab6_profile_has_name(self):
        name = 'Salsabila Maurizka'
        request = HttpRequest()
        response = views.profil(request)
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)

    def test_Lab6_profile_has_npm(self):
        npm = '1706043986'
        request = HttpRequest()
        response = views.profil(request)
        html_response = response.content.decode('utf8')
        self.assertIn(npm, html_response)

    def test_Lab6_profile_has_info(self):
        info = 'Fakultas Ilmu Komputer, Universitas Indonesia'
        request = HttpRequest()
        response = views.profil(request)
        html_response = response.content.decode('utf8')
        self.assertIn(info, html_response)


#daftarbuku.html
    def test_story9_library_url_is_exist(self):
        response=Client().get('/daftarbuku')
        self.assertEqual(response.status_code,301)

    def test_story9_library_return_jsonresponse(self):
        response = Client().get('daftarbuku/api/library/')
        self.assertEqual(response.status_code,404)

    def test_story9_library_using_daftarbuku_func(self):
        found = resolve('/daftarbuku/')
        self.assertEqual(found.func, views.daftarbuku)

#subscribe.html
    def test_story10_subscribe_url_is_exist(self):
        response=Client().get('/subscribe/')
        self.assertEqual(response.status_code,404)

    def test_story10_post_success_and_render_the_result(self):
        name= 'caca'
        email= 'caca@gmail.com'
        password = 'cobaduludehya'
        response_post=Client().post('/add_subs/',{'name':name,'email':email,'password':password})
        self.assertEqual(response_post.status_code,200)

        response = Client().get('/regis/')
        self.assertTemplateUsed(response,'subscribe.html')

    def test_story10_from_validation_for_blank_field(self):
        form= Subscribe_Form(data={'name':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    def test_story10_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/add_subs/',{'name': ''})
        count_data = Subscriber.objects.all().count()
        self.assertEqual(count_data, 0)

    def test_story10_JSON_object_failed_created(self):
        create_model = Subscriber.objects.create(name='caca',email='caca@caca.com',password='cobaduludehya')
        self.assertRaises(IntegrityError)

    #Unit test for story 10 challenge
    def test_story10_url_exist(self):
        response=Client().get('/show_subs/')
        self.assertEqual(response.status_code,200)

    def test_story10_object_does_not_exists_in_the_database(self):
        Client().post('/show_subs/',{'name': ''})
        count_data = Subscriber.objects.all().count()
        self.assertEqual(count_data, 0)

    def test_story10_subs_json_exist(self):
        response = self.client.post('/show_subs/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'subsdata': []}
        )



class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status')
        status = selenium.find_element_by_id('id_status')
        time.sleep(3)
        status.send_keys('Coba-Coba')
        status.submit()
        assert "Coba-Coba" in self.selenium.page_source


    def test_Lab6_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/status')
        hello = selenium.find_element_by_id("hello").text
        assert hello in self.selenium.page_source

        button = selenium.find_element_by_id('delete_button').text
        self.assertIn('Delete', button)

    def test_Lab6_styling(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/status')
        # wait until title is present
        wait = WebDriverWait(selenium, 10)

        selenium.get('http://127.0.0.1:8000/status');
        textColor = selenium.find_element_by_id('hello').value_of_css_property('color');
        self.assertIn('rgba(0, 0, 0, 1)', textColor)

        textFont = selenium.find_element_by_id("hello").value_of_css_property("font-weight");
        self.assertIn('700', textFont)
