from django.db import models
from datetime import datetime
# Create your models here.

class Status(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True, null=True)

class Subscriber(models.Model):
    email = models.CharField(max_length = 30, unique = True, db_index=True)
    name = models.CharField(max_length = 30)
    password = models.CharField(max_length = 15)
