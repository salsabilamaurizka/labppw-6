var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $("#emailForm").keyup(function() {
        checkEmail();
    });

    $("#passForm").keyup(function() {
        $('#statusForm').html('');
        if ($('#passForm').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function() {
        checkEmail()
    });

    $('#submitted').click(function () {
        data = {
            'email' : $('#emailForm').val(),
            'name' : $('#nameForm').val(),
            'password' : $('#passForm').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'add_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('emailForm').value = '';
                document.getElementById('nameForm').value = '';
                document.getElementById('passForm').value = '';

                $('#statusForm').html('');
                checkAll();
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('#emailForm').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    console.log('1');
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submitted').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }

        }
    });
}

function checkAll() {
    if (emailIsValid &&
        $('#nameForm').val() !== '' &&
        $('#passForm').val() !== '' &&
        $('#passForm').val().length > 7) {

        $('#submitted').prop('disabled', false);
    } else {
        $('#submitted').prop('disabled', true);
    }
}
